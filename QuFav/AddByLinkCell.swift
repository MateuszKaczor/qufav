//
//  AddByLinkCell.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 02.10.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import UIKit

protocol AddByLinkCellDelegate {
    func newUrlDidSet(url: String)
}

class AddByLinkCell: UITableViewCell, UITextFieldDelegate {

    
    @IBOutlet weak var urlTextField: UITextField! {
        didSet {
            self.urlTextField.delegate = self
        }
    }
    
    var delegate: AddByLinkCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func urlEditingDidEnd(_ sender: AnyObject) {
        if(delegate != nil){
            //auto enable return key
            delegate?.newUrlDidSet(url: urlTextField.text!)
        }
    }
    

}
