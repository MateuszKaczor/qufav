//
//  YTDescModel.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 28.08.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import Foundation

//      TODO
class Duration {
    
}


class YTDescModel {
    
    var videoTitle: String
    var videoDuration: Duration
    
    
    init(videoTitle: String, videoDuration: Duration){
        self.videoTitle = videoTitle
        self.videoDuration = videoDuration
    }
    
    
}