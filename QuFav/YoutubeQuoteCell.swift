//
//  YoutubeQuoteCell.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 21.08.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import UIKit

protocol YoutubeQuoteCellDelegate: class {
    func playVideo(sender: YoutubeQuoteCell, videoId: String)
}

class YoutubeQuoteCell: UITableViewCell {

    
    weak var delegate:YoutubeQuoteCellDelegate?
    
    var videoId: String = ""
    
    /*
     // MARK: - IBOutlet
    */
    
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var timeProgress: UIProgressView!
   
    /*
     // MARK: - IBAction
     */
    
    @IBAction func play(_ sender: AnyObject) {
        print("test play")
        
        
        delegate?.playVideo(sender: self, videoId: videoId)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
