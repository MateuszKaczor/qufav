//
//  ViewController.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 17.08.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

/*!
    @brief display all quota added by user
*/
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, YTPlayerViewDelegate, YoutubeQuoteCellDelegate {

    
    let youtubePlayerView = YTPlayerView(frame: CGRect(x: -10, y: -10, width: 0, height: 0)) //I co kurwa! Zobaczysz player...
    
    var playedOnce = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        youtubePlayerView.delegate = self
        self.view.addSubview(youtubePlayerView)
        
        youtubePlayerView.load(withVideoId: "")
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.Ź
    }
    
    
    /*!
        //MARK: TableView
    */
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       // return CGFloat.min
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:YoutubeQuoteCell = tableView.dequeueReusableCell(withIdentifier: "YoutubeQuoteCellIdent") as! YoutubeQuoteCell
        
        if indexPath.row == 0 {
            cell.videoId = "MtrJ4Esk3DE"
        }else{
            cell.videoId = "zaDozvg3agI"
        }
        
        cell.delegate = self

        
        return cell
    }
    
    
    /*
     //MARK: YTPlayer
     */
    
    func playVideo(sender: YoutubeQuoteCell, videoId: String) {
        print("Play video with ID: \(videoId)")
        
            //API key -  AIzaSyCf3fG52XqgXuKLrKHKcMeemyuxth_WUJ4
            youtubePlayerView.cueVideo(byId: videoId, startSeconds: 0.0, suggestedQuality: YTPlaybackQuality.default)

            youtubePlayerView.playVideo()
        
        
    }

    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch(state) {
        case YTPlayerState.unstarted:
            //print("Unstarted")
            
            break
        case YTPlayerState.queued:
            //print("Ready to play")
            
            break
        case YTPlayerState.playing:
            //print("Video playing")
            
            break
        case YTPlayerState.paused:
            //print("Video paused")
            //youtubePlayerView.stopVideo()
          /*  DispatchQueue.main.asynchronously(execute: {
                self.youtubePlayerView.stopVideo()
            })*/
            break
        default:
            break
        }
    }
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        print("ready")
        
     /*   dispatch_async(DispatchQueue.main, {
          //  self.youtubePlayerView.playVideo()
        })
        */
        // youtubePlayerView.playVideo()
    }


    /*
     //MARK: unwid segues
    */
    @IBAction func cancelAddingQuote(segue: UIStoryboardSegue){
        print("cancel")
    }
    
    @IBAction func saveQuote(segue: UIStoryboardSegue){
        print("save")
    }
}

