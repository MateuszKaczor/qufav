//
//  SelectTimeCell.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 02.10.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import UIKit

class SelectTimeCell: UITableViewCell {

    
    @IBOutlet weak var timeStartButton: UIButton!
    
    @IBOutlet weak var timeEndButton: UIButton!
    
    @IBOutlet weak var videoTextButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
