//
//  YoutubeService.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 28.08.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import Foundation


protocol Gettable {
    associatedtype Data
    
    func get(completionHandler: (Result<Data>) -> Void)
}

//Test
enum YTError: Error {
    case NotResponding
}


enum Result<T> {
    case Succes(T)
    case Failure(Error)
}
/*
// Asynchronous Http call to your api url, using NSURLSession:
NSURLSession.sharedSession().dataTaskWithURL(NSURL(string: "http://api.site.com/json")!, completionHandler: { (data, response, error) -> Void in
    // Check if data was received successfully
    if error == nil && data != nil {
        do {
            // Convert NSData to Dictionary where keys are of type String, and values are of any type
            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String:AnyObject]
            // Access specific key with value of type String
            let str = json["key"] as! String
        } catch {
            // Something went wrong
        }
    }
}).resume()*/

struct YoutubeService: Gettable {
    
    func get(completionHandler: (Result<[YTDescModel]>) -> Void) {
        
        completionHandler(Result.Failure(YTError.NotResponding))
        
    }
    
}
