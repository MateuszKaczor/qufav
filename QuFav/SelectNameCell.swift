//
//  SelectNameCell.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 02.10.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import UIKit

class SelectNameCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            self.nameTextField.delegate = self
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
