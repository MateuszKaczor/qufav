//
//  AddQuoteViewController.swift
//  QuFav
//
//  Created by Mateusz Kaczor on 17.08.2016.
//  Copyright © 2016 Mateusz Kaczor. All rights reserved.
//

import UIKit

class AddQuoteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AddByLinkCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var videoIsOK = false {
        didSet {
            
            print("reload data")
            
            tableView.reloadData()
        }
    }
    
    var ytData = [YTDescModel]() {
        didSet{
            //reload table view if
            print("did set ytData")
            
            // if data is ok, user must set other information ()
            videoIsOK = true
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        //tmp
        getYTData(fromService: YoutubeService())
        
        
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("PrepareForSegue: \(segue.identifier)")
        
        //TODO: chek data before save
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    /*
     //MARK - get data from youtube
    */

    func getYTData<Service: Gettable>(fromService service: Service) where Service.Data == [YTDescModel] {
        
        service.get() { [weak self] result in
            
            switch result {
            case .Succes(let ytDescModel):
                    self?.ytData = ytDescModel
                
            case .Failure(let error):
                    self?.showError(error: error)
                
            }
            
        }
    }
    
    
    /*
     //MARK - table view
    */

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if videoIsOK {
            return 4
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "addByLinkCell") as! AddByLinkCell
            
            (cell as! AddByLinkCell).delegate   = self
        }else if indexPath.row == 1  {
            cell = tableView.dequeueReusableCell(withIdentifier: "selectTimeCell") as! SelectTimeCell
            
           // (cell as! SelectTimeCell).delegate   = self
        }else if indexPath.row == 2  {
            cell = tableView.dequeueReusableCell(withIdentifier: "selectNameCell") as! SelectNameCell
            
        }else if indexPath.row == 3  {
            cell = tableView.dequeueReusableCell(withIdentifier: "selectLibray") as! SelectLibray
            
        }
        
        
        return cell
    }
    
    /*
     //MARK - cells delegates
     */
    
    func newUrlDidSet(url: String) {
        print("new url: \(url)")
        
        //validate url...
            videoIsOK = true
    }
    
    
    /*
     //MARK - errors
     */
    //TODO
    func showError(error: Error) {
        print("error: \(error)")
    }



}
